import { Component, OnDestroy } from '@angular/core';
import { buttonsRows } from '../utils/buttons';
import { Button } from '../utils/Button';
import { ComponentStore } from '@ngrx/component-store';
import { Subscription } from 'rxjs';

interface CalculatorState {
  title: string;
  sumHistory: string;
  sum: string;
}

const initialState = {
  title: '',
  sumHistory: 'Calculator',
  sum: '0',
};

@Component({
  selector: 'app-calculator',
  templateUrl: 'calculator.page.html',
  styleUrls: ['calculator.page.scss'],
})

export class CalculatorPage implements OnDestroy {
  public state: CalculatorState;
  public buttonsRows: Button[][] = buttonsRows;
  private componentStoreSub$$: Subscription;

  constructor(private readonly componentStore: ComponentStore<CalculatorState>) {
    this.componentStore.setState(initialState);
    this.componentStoreSub$$ = this.componentStore.state$.subscribe(state => this.state = state);
  }

  /**
   * Destroy and cancel
   */
  public ngOnDestroy() {
    this.componentStoreSub$$.unsubscribe();
  }

  /**
   * Click on buttons
   * @param event 
   * @param button 
   * @returns 
   */
  public handleClick = (event: MouseEvent, button: Button) => {

    const tempSumHistory = this.state.sumHistory.replace(initialState.sumHistory, '');

    if (button.value === '=') {
      return this.calculate();
    }
    if (button.value === 'C') {
      return this.reset();
    }
    if (button.value === 'Del') {
      this.backspace();
      return this.calculate();
    }
    if (button.value === '%') {
      this.addpercent();
      return this.calculate();
    }
    if (button.value === '+/-') {
      this.addPlusMinus();
      return this.calculate();
    }

    this.componentStore.setState({
      ...this.state,
      sumHistory: tempSumHistory + button.value,
    });
    button.shake = true;
    setTimeout(() => {
      button.shake = false;
    }, 500);
    return this.calculate();
  };

  /**
   * Reset
   */
  private reset = () => {
    this.componentStore.setState(initialState);
  };

  /**
   * Backspace
   * @returns 
   */
  private backspace = () => {
    const sumHistory = this.state.sumHistory.substr(0, this.state.sumHistory.length - 1);
    return this.componentStore.setState({...this.state, sumHistory});
  };
  
  /**
  * Evaluate a value
  * @param value 
  * @returns 
  */
  private evaluate = (value): number => Function('"use strict"; return (' + value + ')')();

  /**
  * Format a number
  * @param value 
  * @returns 
  */
  private formatNumber = (value: string) => {
    const [whole, fraction] = value.split('.');
    const formattedWhole = whole.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return fraction ? [formattedWhole, fraction].join('.') : formattedWhole;
  };

  /**
   * Add Plus and Minus
   * @returns 
   */
  private addPlusMinus(){
    let sumHistory = this.state.sum;
    if (Math.sign(parseInt(this.state.sum, 0)) === 1) {
      const sign = -Math.abs(parseInt(this.state.sum, 0));
      sumHistory = sign.toString();
    }
    if (Math.sign(parseInt(this.state.sum, 0)) === -1) {
      const sign = Math.abs(parseInt(this.state.sum, 0));
      sumHistory = sign.toString();
    }
    return this.componentStore.setState({...this.state, sumHistory});
  }

  /**
   * Get percent of a number
   */
  private addpercent() {
    if(Math.sign(parseInt(this.state.sum, 0)) > 0){
      const percent = parseInt(this.state.sum, 0) / 100;
      const sumHistory = percent.toString();
      return this.componentStore.setState({...this.state, sumHistory});
    }
  }

  /**
   * Calculate numbers
   * @returns 
   */
  private calculate = () => {
    try {
      const sum = this.evaluate(this.state.sumHistory);
      const formattedSum = this.formatNumber(sum.toString());
      return this.componentStore.setState({
        ...this.state,
        title: initialState.sumHistory,
        sum: formattedSum,
      });
    } catch (e) {}
  };

}
