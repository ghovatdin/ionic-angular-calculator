import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { CalculatorPage } from './calculator.page';
import { HomePageRoutingModule } from './calculator-routing.module';
import { ComponentStore } from '@ngrx/component-store';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule
  ],
  declarations: [CalculatorPage],
  providers: [ComponentStore]
})
export class HomePageModule {
}
