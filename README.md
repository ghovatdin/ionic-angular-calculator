# Ionic Angular Calculator
An example of a Calculator UI built with Ionic Angular

### Included in this Ionic Angular UI
* Fully functional Calculator
* Custom buttons
* History of calculations
* Animations on button clicks
* Automatic calculation
* Use of stock Ionic components
* Ionic CSS utilities

### To run

```javascript
npm install
ionic serve
```

Alternatively, you can add the iOS, Android platform and run natively.
